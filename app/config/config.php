<?php

/**
* @package	Configuration
*/

return [

	"level"					=> 1,
	"develop"				=> true,
	"down"					=> false,
	"server"				=> null,
	"timezone"				=> "America/Mexico_City",
	"lang"					=> "es-ES",
	"auth"					=> "@{url.home.login}",
	"session_length"		=> 3600,
	"user_session_length"	=> 86400,
	"cookie_domain"			=> null,
	"cookie_length"			=> 3600

];