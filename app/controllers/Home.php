<?php

/**
* @package	Home
* @version	1.0
* @author	You
* @since	2015-05-20
* @see		2015-12-02
*/

class Home
{
	public function Index()
	{
		Response::Send("Hello Elephant!");
	}

	public function HelloWorld()
	{
		Response::View();
	}
}