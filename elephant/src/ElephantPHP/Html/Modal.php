<?php

/**
* @package	Modal
* @version	2.3
* @author	DavidBeru
* @since	2013-08-25
* @see		2015-01-31
*/

class Modal
{
	public static function Make($_title = "Untitled", $_content = null, $_class = null)
	{
		$class = is_string($_class) ? " {$_class}" : null;

		return View::Make("@{path}.elephant.views.modal.modal", array(
			"name" => Hash::Make(),
			"title" => $_title,
			"content" => $_content,
			"class" => $class));
	}

	public static function Error($_content = null)
	{
		return View::Make("@{path}.elephant.views.modal.modal", array(
			"name" => Hash::Make(),
			"title" => "Error",
			"content" => $_content));
	}
}