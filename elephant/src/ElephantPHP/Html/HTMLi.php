<?php

/**
* @package	HTMLi
* @version	1.7.2
* @author	DavidBeru
* @since	2015-01-27
* @see		2015-10-27
*/

class HTMLi
{
	private $tag = null;
	private $attr = array();
	private $value = null;
	private $close = null;

	public function __tostring()
	{
		return $this->OutPut();
	}

	public function OutPut()
	{
		$attr = null;

		if (is_array($this->attr))
		{
			foreach ($this->attr as $key => $value)
			{
				if (is_integer($key))
				{
					$attr .= " {$value}=\"{$value}\"";
				}
				else
				{
					$attr .= " {$key}=\"{$value}\"";
				}
			}
		}

		return "<{$this->tag}{$attr}{$this->value}{$this->close}";
	}

	public function Attr($_key = null, $_value = null)
	{
		if (is_array($_key))
		{
			foreach ($_key as $key => $value)
			{
				$this->Attr($key, $value);
			}
		}
		else
		{
			$this->attr[$_key] = $_value;
		}

		return $this;
	}

	public function Id($_id = null)
	{
		$this->Attr("id", $_id);

		return $this;
	}

	public function Name($_name = null)
	{
		$this->Attr("name", $_name);

		return $this;
	}

	public function Required()
	{
		$this->Attr("required", "required");

		return $this;
	}

	public function Disabled()
	{
		$this->Attr("disabled", "disabled");

		return $this;
	}

	public function Focus()
	{
		$this->Attr("autofocus", "autofocus");

		return $this;
	}

	public function PlaceHolder($_placeholder = null)
	{
		$this->Attr("placeholder", $_placeholder);
		$this->Title($_placeholder);

		return $this;
	}

	public function Title($_title = null)
	{
		$this->Attr("title", $_title);

		return $this;
	}

	public function Min($_length = null)
	{
		$this->Attr("min", $_length);

		return $this;
	}

	public function Max($_length = null)
	{
		$this->Attr("max", $_length);

		return $this;
	}

	public function MinLength($_length = null)
	{
		$this->Attr("minlength", $_length);

		return $this;
	}

	public function MaxLength($_length = null)
	{
		$this->Attr("maxlength", $_length);

		return $this;
	}

	public function Width($_width = null)
	{
		$this->Attr("width", $_width);

		return $this;
	}

	public function Height($_height = null)
	{
		$this->Attr("height", $_height);

		return $this;
	}

	public function Style($_style = null)
	{
		$this->Attr("style", $_style);

		return $this;
	}

	public function Data($_key = null, $_value = null)
	{
		if (is_array($_key))
		{
			foreach ($_key as $key => $value)
			{
				$this->Attr("data-{$key}", $value);
			}
		}
		else
		{
			$this->Attr("data-{$_key}", $_value);
		}

		return $this;
	}

	public function Open($_tag = null, $_value = null, $_attr = array(), $_close = true)
	{
		#if (is_string($_value) || is_null($_value))
		#{
			$tag = String::Lower($_tag);
			$value = null;

			switch ($tag)
			{
				case "img":
				case "iframe":
					$_attr["src"] = $_value;
					break;
				case "input":
					$_attr["value"] = $_value;
					break;
				default:
					$value = ">{$_value}";
					break;
			}

			$this->tag = $tag;
			$this->attr = $_attr;
			$this->value = $value;
			$this->close = ($_close === true) ? $this->Close($tag) : null;

			return $this;
		#}
		#else
		#{
		#	Response::Error(500, "Se esperaba un string en el parametro _value de la clase HTMLi");
		#}
	}

	public function Close($_tag = null)
	{
		$tag = String::Lower($_tag);

		switch ($tag)
		{
			case "img":
			case "input":
				$close = '>';
				break;
			default:
				$close = "</{$tag}>";
				break;
		}

		return $close;
	}

	public function BreadCrumbs($_class = "breadcrumbs")
	{
		$bread = URI::BreadCrumbs();
		$menu = null;

		foreach ($bread as $key => $value)
		{
			$menu .= HTML::Open("li", HTML::Open('a', $value)->Attr("href", $key));
		}

		return is_null($menu) ? null : HTML::Open("ul", $menu)->Attr("class", $_class);
	}
}