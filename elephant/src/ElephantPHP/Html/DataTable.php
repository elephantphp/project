<?php

/**
* @package	DataTable
* @version	2.5
* @author	DavidBeru
* @since	2014-12-23
* @see		2015-01-18
*/

class DataTable
{
	public static function Make($_name = null, $_control = null)
	{
		$control = Model::Get($_control, "datatable");

		if ($control)
		{
			$cl = $control["control"];
			$args = $control["args"];
			$new = isset($cl["new"]) ? $cl["new"] : false;
			$search = isset($cl["search"]) ? $cl["search"] : "";
			$table = self::Get(
				$_name,
				$_control,
				$cl,
				$args);

			if (is_null($table))
			{
				return View::Make("@{path}.elephant.views.datatable.datatable-empty", array(
					"name" => $_name,
					"new" => $new));
			}
			else
			{
				return View::Make("@{path}.elephant.views.datatable.datatable", array(
					"title" => $cl["name"],
					"name" => $_name,
					"control" => bin2hex($_control),
					"new" => $new,
					"search" => $search,
					"table" => $table));
			}
		}
		else
		{
			return "Error, no existe el model '{$_control}' para un DateTable";
		}
	}

	public static function Get($_name = null, $_control = null, $_cl = array(), $_args = array())
	{
		$name = isset($_cl["name"]) ? $_cl["name"] : "DataTable";
		$sql = isset($_cl["sql"]) ? $_cl["sql"] : null;
		$table = isset($_cl["table"]) ? $_cl["table"] : null;
		$primary = isset($_cl["primary"]) ? $_cl["primary"] : null;
		$limit = isset($_cl["limit"]) ? $_cl["limit"] : 5;
		$where = isset($_cl["where"]) ? $_cl["where"] : null;
		$where_union = isset($_cl["where_union"]) ? $_cl["where_union"] : null;
		$having = isset($_cl["having"]) ? $_cl["having"] : null;
		$having_union = isset($_cl["having_union"]) ? $_cl["having_union"] : null;
		$order = isset($_cl["order"]) ? $_cl["order"] : null;
		$fields = isset($_cl["fields"]) ? $_cl["fields"] : null;
		$db = isset($_cl["db"]) ? $_cl["db"] : null;
		$actions = isset($_cl["actions"]) ? $_cl["actions"] : "";
		$search = isset($_cl["search"]) ? $_cl["search"] : "";
		$delete	= isset($_cl["delete"]) ? $_cl["delete"] : false;

		$val = Request::Post("elephant_control_value", null);
		$page = Request::Post("elephant_control_page", 1);
		$limit = Request::Post("elephant_control_limit", $limit);
		$page = ($page - 1) * $limit;
		$sqllimit = "$page,$limit";

		if ($where || $where_union)
		{
			if ($where_union)
			{
				$where = empty($val) ? null : " $where_union";
			}
			else
			{
				$where = empty($val) ? null : "WHERE $where";
			}
		}

		if ($having || $having_union)
		{
			if ($having_union)
			{
				$having = empty($val) ? null : " $having_union";
			}
			else
			{
				$having = empty($val) ? null : "HAVING $having";
			}
		}

		$order = is_null($order) ? null : "ORDER BY $order";

		if (is_null($sql))
		{
			$sql = Fn::Args("SELECT * FROM $table $where $having", $_args);
		}
		else
		{
			$sql = Fn::Args("$sql $where $having", $_args);
		}

		$query = DB::Select($sql, $db);
		$rows_all = $query->Rows();
		$query = DB::Select("$sql $order LIMIT $sqllimit", $db);
		$rows = $query->Rows();

		if ($rows)
		{
			$values = array();

			while ($row = $query->Result())
			{
				$result = array();

				foreach ($fields as $key => $value)
				{
					$result[] = $row[$value];
				}

				$values[$row[$primary]] = $result;
			}

			$return = array(
				"control" => bin2hex($_control),
				"name" => $_name,
				"rows" => $rows,
				"rows_all" => $rows_all,
				"page" => $page,
				"limit" => $limit,
				"columns" => array_keys($fields),
				"values" => $values,
				"search" => $search,
				"actions" => $actions,
				"delete" => $delete);

			return View::Make("@{path}.elephant.views.datatable.datatable-table", array(
				"elephant_cl" => $return));
		}
		else
		{
			if ($val)
			{
				return View::Make("@{path}.elephant.views.datatable.datatable-notfound");
			}
			else
			{
				return null;
			}
		}
	}
}