<?php

/**
* @package	DataList
* @version	2.3
* @author	DavidBeru
* @since	2014-12-23
* @see		2015-01-18
*/

class DataList
{
	public static function Make($_name = null, $_control = null, $_value = null, $_attr = array())
	{
		$_attr["data-fw-control-type"] = "datalist";
		$_attr["data-fw-control-name"] = $_name;
		$_attr["data-fw-control"] = bin2hex($_control);
		$return = Form::Hidden($_name, $_value);
		$return .= Form::Text("datalist_text_" . $_name, $_value, $_attr);
		$return .= HTML::Open("div", null, array(
			"id" => "datalist_{$_name}",
			"class" => "datalist"));

		return $return;
	}

	public static function Get($_name, $_control, $_sql, $_fields, $_db = null)
	{
		$query = DB::Select($_sql, $_db);

		if ($query->Rows())
		{
			$return = "";

			while ($row = $query->Result())
			{
				$return .= "<li data-fw-type=\"datalist\" data-fw-name=\"{$_name}\" data-fw-control=\"{$_control}\" data-fw-value=\"" . $row[$_fields[0]] . "\">";
				$return .= $row[$_fields[1]];
				$return .= "</li>";
			}

			$return = "<ul>" . $return . "</ul>";
		}

		return isset($return) ? $return : "No";
	}
}