<?php

/**
* @package	Paginator
* @version	1.1
* @author	DavidBeru
* @since	2013-09-13
* @see		2015-02-14
*/

class Paginator
{
	public static function Ajax($_control = null, $_name = null, $num = 1, $perpage = 10, $domain = null)
	{
		if ($num > 1)
		{
			$page  = Request::Post("elephant_control_page", 1);
			$perpage = ($perpage == 0) ? 1 : $perpage;
			$total = ($num === 0) ? 0 : ceil($num / $perpage);;
			$prev = $page - 1; 
			$next = $page + 1;
			$prev = ($page < 2) ? 1 : $prev;
			$next = (($page + 1) > $total) ? $total : $next;
			$first = null;
			$before = null;
			$after = null;
			$last = null;

			# First
			if ($page > 2)
			{
				$first = '<li title="Ir a la primera página">
					<a data-value=1>Primero</a>
				</li>';
			}

			# Before
			if ($page > 1)
			{
				$before = '<li title="Página anterior">
					<a data-value='.$prev.'>Anterior</a>
				</li>';
			}

			# After
			if ($page < $total)
			{
				$after = '<li title="Página siguiente">
					<a data-value='.$next.'>Siguiente</a>
				</li>';
			}

			# Last
			if ($page < ($total - 1))
			{
				$last = '<li title="Ir a la ultima página">
					<a data-value='.$total.'>Ultimo</a>
				</li>';
			}

			if ($total == 1)
			{
				return "";
			}
			else
			{
				return '
				<ul class="paginator" data-fw-control-type="datatable-paginator" data-fw-control="'.$_control.'" data-fw-control-name="'.$_name.'" data-perpage="'.$perpage.'">
					'.$first.$before.'
					<li title="Página actual">
						<form action="'.$domain.'" method="GET">
							<input type="hidden" name="perpage" value="'.$perpage.'">
							<input type="text" name="page" value="'.$page.'" class="paginator">
						</form>
					</li>
					<li title="Total de páginas">
						<a data-value='.$total.'>de '.$total.' páginas</a>
					</li>
					'.$after.$last.'
				</ul>';
			}
		}
		else
		{
			return null;
		}
	}

	public static function Input($num = 1, $perpage = 10, $domain = null)
	{
		if ($num > 1)
		{
			$page = isset($_GET["page"]) ?$_GET["page"]: 1;
			$total = ceil($num / $perpage);
			$prev = $page - 1; 
			$next = $page + 1;
			$prev = ($page < 2) ? 1 : $prev;
			$next = (($page + 1) > $total) ? $total : $next;
			$first = null;
			$before = null;
			$after = null;
			$last = null;
			$li = null;
			$html = new HTML();

			# First
			if ($page > 2)
			{
				$a = $html->Open("a", "Primero", array("href" => "{$domain}?page=1"));
				$first = $html->Open("li", $a, array("title" => "Ir a la primera página"));
			}

			# Before
			if ($page > 1)
			{
				$a = $html->Open("a", "Anterior", array("href" => "{$domain}?page={$prev}"));
				$before = $html->Open("li", $a, array("title" => "Página anterior"));
			}

			# After
			if ($page < $total)
			{
				$a = $html->Open("a", "Siguiente", array("href" => "{$domain}?page={$next}"));
				$after = $html->Open("li", $a, array("title" => "Página siguiente"));
			}

			# Last
			if ($page < ($total - 1))
			{
				$a = $html->Open("a", "Último", array("href" => "{$domain}?page={$total}"));
				$last = $html->Open("li", $a, array("title" => "Ir a la ultima página"));
			}

			$input = Form::Text("page", $page, array("class" => "paginator"));
			$form = Form::Open($domain, "GET") . $input . Form::Close();
			$li .= $html->Open("li", $form, array("title" => "Página actual"));
			$a = $html->Open("a", "de {$total} páginas", array("href" => "{$domain}?page={$total}"));
			$li .= $html->Open("li", $a, array("title" => "Total de páginas"));

			return $html->Open("ul", "{$first}{$before}{$li}{$after}{$last}", array(
				"class" => "paginator"));
		}
		else
		{
			return null;
		}
	}
}