<?php

/**
* @package	Hash
* @version	2.2
* @author	DavidBeru
* @since	2013-07-24
* @see		2015-10-06
*/

class Hash
{
	public static function Make($_string = null, $_type = false)
	{
		if ($_string == null || $_type === true)
		{
			$step1 = Date::DateTime() . rand(0, 10000) . rand(0, 1000);
		}
		else
		{
			$step1 = App::Secrets("hash") . $_string;
		}

		$step2 = sha1($step1 . md5($_string));
		$step3 = hash("md2", $step2, true);
		$step4 = sha1("|#€7`¬23ads4ook12@Hks9_-.d78djd?sjks¿!qj2kkxdksj?*ds+[}xkx]" . hash("md2", $step3, true));

		return $step4;
	}

	public static function Generate($_length = 40)
	{
		if (is_integer($_length))
		{
			$lower = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
			$upper = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
			$number = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
			$hash = null;

			for ($i = 0; $i < $_length; $i++)
			{
				switch (rand(0, 2))
				{
					case 0:
						$hash .= $lower[rand(0, 25)];
						break;
					case 1:
						$hash .= $upper[rand(0, 25)];
						break;
					case 2:
						$hash .= $number[rand(0, 9)];
						break;
				}
			}

			return $hash;
		}
		else
		{
			return false;
		}
	}

	public static function HexaToInt($_string = null)
	{
		$lower = array('a', 'b', 'c', 'd', 'e', 'f', 'g');
		$upper = array('A', 'B', 'C', 'D', 'E', 'F', 'G');
		$string = null;
		$int = 0;

		foreach (str_split(trim($_string)) as $key => $value)
		{
			if (in_array($value, $lower))
			{
				$val = array_search($value, $lower);
			}
			else if (in_array($value, $upper))
			{
				$val = array_search($value, $lower);
			}
			else
			{
				$val = $value;
			}

			$int += (int) $val;
			$string .= $val;
		}

		return $int;
	}
}