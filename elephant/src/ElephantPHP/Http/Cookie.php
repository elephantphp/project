<?php

/**
* @package	Cookie
* @version	1.3.5
* @author	DavidBeru
* @since	2013-07-24
* @see		2015-04-29
*/

class Cookie
{
	public static function Make($_key = null, $_value = null, $_time = ELEPHANTPHP_APP_COOKIE_LENGTH, $_domain = ELEPHANTPHP_APP_COOKIE_DOMAIN)
	{
		if ($_key)
		{
			if (is_string($_time))
			{
				$array = explode(' ', $_time);

				switch($array[1])
				{
					case"second":
					case"seconds":

						$time = $array[0];

						break;
					case"minute":
					case"minutes":

						$time = $array[0] * 60;

						break;
					case"hour":
					case"hours":

						$time = $array[0] * 3600;

						break;
					case"day":
					case"days":

						$time = $array[0] * (3600 * 24);

						break;
					case"month":
					case"months":

						$time = $array[0] * (3600 * 24 * 30);

						break;
					case"year":
					case"years":

						$time = $array[0] * (3600 * 24 * 365);

						break;
					default:

						$time = 0;

						break;
				}
			}
			else
			{
				$time = $_time;
			}

			$domain = is_null($_domain) ? URI::Domain() : $_domain;
			$domain = ($domain == "localhost") ? null : $domain;

			if (is_array($_value))
			{
				foreach($_value as $key => $val)
				{
					@setcookie("{$_key}[{$key}]", $val, time() + $time, '/', $domain);
				}

				return true;
			}
			else
			{
				return @setcookie($_key, $_value, time() + $time, '/', $domain);
			}
		}
		else
		{
			return null;
		}
	}

	public static function Has($_key = null)
	{
		if ($_key)
		{
			return isset($_COOKIE[$_key]) ? true : false;
		}
		else
		{
			return null;
		}
	}

	public static function Get($_key = null, $_value = null)
	{
		if ($_key)
		{
			$vector = String::Split($_key, '.');

			if (self::Has($vector[0]))
			{
				if (isset($vector[1]))
				{
					return $_COOKIE[$vector[0]][$vector[1]];
				}
				else
				{
					return $_COOKIE[$vector[0]];
				}
			}
			else
			{
				return ($_value) ? $_value : false;
			}
		}
		else
		{
			return false;
		}
	}

	public static function Forget($_key = null)
	{
		if (is_null($_key))
		{
			return null;
		}
		else
		{
			if (self::Has($_key))
			{
				return self::Make($_key, null, - 3600);
			}
			else
			{
				return false;
			}
		}
	}

	public static function Flush()
	{
		if (is_array($_COOKIE))
		{
			foreach ($_COOKIE as $key => $value)
			{
				self::Forget($key);
			}

			return true;
		}
		else
		{
			return null;
		}
	}
}