<?php

/**
* @package	Redirect
* @version	1.5.1
* @author	DavidBeru
* @since	2013-07-27
* @see		2015-04-29
*/

class Redirect
{
	public static function Home($_route = null, $_parameters = array(), $_identifier = null)
	{
		header("Location: " . URI::Home($_route, $_parameters, $_identifier));
	}

	public static function Reload()
	{
		header("Location: " . URI::Current());
	}

	public static function URI($_url = null, $_parameters = array(), $_identifier = null)
	{
		if ($_url)
		{
			header("Location: " . URI::Parameters($_url, $_parameters, $_identifier));
		}
		else
		{
			return null;
		}
	}

	public static function Auth($_parameters = array(), $_identifier = null)
	{
		header("Location: " . URI::Auth($_parameters, $_identifier));
	}
}