<?php

/**
* @package	Response
* @version	2.1
* @author	DavidBeru
* @since	2013-11-30
* @see		2015-06-19
*/

class Response
{
	private static $error = false;

	private static function Set($_content = null, $_header = null, $_type = null)
	{
		if (is_array($_header))
		{
			foreach ($_header as $key => $value)
			{
				header("{$key}: {$value}");
			}
		}
		else if (is_null($_header))
		{
			header("Content-Type: text/html; charset=UTF-8");
		}
		else
		{
			header($_header);
		}

		if ($_type !== "file")
		{
			ob_start("Fn::ClearHTML");
		}

		$content = (string) $_content;

		return exit($content);
	}

	public static function Send($_content = null)
	{
		return self::Set($_content);
	}

	public static function Text($_content = null)
	{
		return self::Set($_content, array("Content-Type" => "text/plain; charset=UTF-8"));
	}

	public static function Error($_code = null, $_message = null, $_file = null, $_line = null, $_link = null)
	{
		if (Validator::Int($_code))
		{
			if (self::$error === false)
			{
				self::$error = true;
				ob_get_clean();

				if (Request::AJAX())
				{
					$info = "Code: {$_code}\n
					Error message: {$_message}\n
					File: {$_file}\n
					Line: {$_line}\n";

					return self::Send($info);
					return self::JSON(array(
						"code" => $_code,
						"message" => $_message,
						"file" => $_file,
						"line" => $_line));
				}
				else
				{
					return self::View("@{path}.elephant.views.error", array(
						"code" => $_code,
						"message" => $_message,
						"file" => $_file,
						"line" => $_line,
						"link" => $_link));
				}
			}
		}
		else
		{
			return self::Send("The code should be a interger");
		}
	}

	public static function JSON($_data = null)
	{
		if (is_array($_data))
		{
			return self::Set(JSON::Make($_data), array(
				"Content-Type" => "text/json"));
		}
		else
		{
			return false;
		}
	}

	public static function CSV($_data = null, $_filename = "file-csv")
	{
		if (is_array($_data))
		{
			return self::Set(CSV::Make($_data), array(
				"Content-Type" => "application/vnd.ms-excel",
				"Content-Disposition" => "attachment; filename='{$_filename}.csv'"));
		}
		else
		{
			return false;
		}
	}

	public static function View($_view = null, $_assign = array())
	{
		return self::Set(View::Make($_view, $_assign));
	}

	public static function Modal($_title = "Untitled", $_content = null, $_class = null)
	{
		return self::Set(Modal::Make($_title, $_content, $_class));
	}

	public static function File($_route = null, $_filename = null, $_storage = null)
	{
		if ($_route == '/')
		{
			return self::Send("The route to file is empty");
		}
		else
		{
			$file = File::Get($_route, $_storage);

			if ($file)
			{
				$path = File::Storage($_storage) . $_route;
				$info = pathinfo($path);
				$filename = ($_filename) ? $_filename : $info["filename"];
				$ext = File::GetExt($_route);
				$type = File::GetType($ext);

				return self::Set(
					$file,
					array(
						"Content-Type" => $type,
						"Content-Disposition" => "inline; filename='{$filename}.{$ext}'"),
					"file");
			}
			else
			{
				return self::Send("The file not exist");
			}
		}
	}

	public static function Download($_route = null, $_filename = null, $_storage = null)
	{
		$file = File::Get($_route, $_storage);

		if ($file)
		{
			$path = File::Storage($_storage) . $_route;
			$info = pathinfo($path);
			$filename = ($_filename) ? $_filename : $info["filename"];
			$ext = File::GetExt($_route);
			$type = File::GetType($ext);

			return self::Set(
				$file,
				array(
					"Content-Description" => "File Transfer",
					"Content-Type" => $type,
					"Content-Disposition" => "attachment; filename='{$filename}.{$ext}'",
					"Content-Length" => filesize($path),
					"Expires" => 0,
					"Cache-Control" => "must-revalidate",
					"Pragma" => "Public"),
				"file");
		}
		else
		{
			return self::Send("The file not exist");
		}
	}
}