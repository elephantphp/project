<?php

/**
* @package	JSON
* @version	2.2
* @author	DavidBeru
* @since	2014-04-10
* @see		2015-01-03
*/

class JSON
{
	public static function Make($_data = null)
	{
		if (is_array($_data))
		{
			return @json_encode($_data);
		}
		else
		{
			return false;
		}
	}

	public static function Get($_data = null)
	{
		if ($_data)
		{
			if (Validator::URL($_data))
			{
				$data = @file_get_contents($_data);

				if ($data)
				{
					return @json_decode($data, true);
				}
				else
				{
					return false;
				}
			}
			else
			{
				return @json_decode($_data, true);
			}
		}
		else
		{
			return null;
		}
	}
}