<?php

/**
* @package	Lang
* @version	1.6
* @author	DavidBeru
* @since	2014-12-23
* @see		2015-06-16
*/

class Lang
{
	private static $locale = ELEPHANTPHP_APP_LANG;
	private static $lang_code = array(
		"English (United States)" => "en-US",
		"Spanish (Spain)" => "es-ES");

	private static function SetLocale($_locale = null)
	{
		self::$locale = $_locale;
	}

	public static function Get($_string = null, $_vars = array(), $_lang_code = null)
	{
		$lang_code = is_null($_lang_code) ? self::$locale : $_lang_code;

		if (in_array($lang_code, self::$lang_code))
		{
			$string = String::Lower($_string);
			$split = String::Split($string, '.');

			if (count($split) > 1)
			{
				$file = $split[0];

				if ($file === "@{elephantphp}")
				{
					$file = $split[1];
					$path = ELEPHANTPHP_PATH . "elephant/lang/{$lang_code}/{$file}.php";
					unset($split[0]);
					unset($split[1]);
				}
				else
				{
					$file = String::Lower(($file == "@{controller}") ? ElephantPHP::GetController() : $file);
					$path = ELEPHANTPHP_PATH . "app/lang/{$lang_code}/{$file}.php";
					unset($split[0]);
				}

				if (file_exists($path))
				{
					ob_start();
					$content = require $path;
					ob_get_clean();

					if (is_array($content))
					{
						$val = $content;

						foreach ($split as $key => $value)
						{
							$val = isset($val[$value]) ? $val[$value] : false;
						}

						if (is_string($val))
						{
							if (is_array($_vars))
							{
								foreach ($_vars as $key => $value)
								{
									if (is_string($value) || Validator::Int($value))
									{
										$val = str_replace("@{{$key}}", $value, $val);
									}
									else
									{
										$val = str_replace("@{{$key}}", "{{error format}}", $val);
									}
								}

								return $val;
							}
							else
							{
								return $val;
							}
						}
						else
						{
							return "{{{$string}}}";
						}
					}
					else
					{
						return "Invalid structure";
					}
				}
				else
				{
					return "The file does not exist";
				}
			}
			else
			{
				return "Over 1 parameter expected";
			}
		}
		else
		{
			return "The lang code is not allowed";
		}
	}
}