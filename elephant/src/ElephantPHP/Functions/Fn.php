<?php

/**
* @package	Fn
* @version	2.9
* @author	DavidBeru
* @since	2013-07-26
* @see		2015-04-29
*/

class Fn
{
	private static $args = array();
	private static $regex_args = '/@\{arg:[ 0-9]{1,100}\}/i';

	public static function Args($_value = null, $_args = array())
	{
		self::$args = $_args;

		$value = preg_replace_callback(self::$regex_args, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_args, $value))
				{
					$v = trim(substr($value, 6, -1));
					$val = isset(self::$args[$v]) ? self::$args[$v] : null;
				}
			}

			return $val;

		}, $_value);

		return $value;
	}

	public static function Avg($_array = null)
	{
		if (is_array($_array))
		{
			$val = 0;

			foreach ($_array as $key => $value)
			{
				$val += $value;
			}

			return @round($val / count($_array), 2);
		}
		else
		{
			return 0;
		}
	}

	public static function Markup($_subject = "", $_search = null, $_replace = "text-markup")
	{
		if ($_search != null)
		{
			$n = array(' ', 'ú', 'Ú');
			$y = array('', 'u', 'U');

			#$_subject = str_ireplace($n, $y, $_subject);
			$_search = str_ireplace($n, $y, $_search);

			return preg_replace("/(" . $_search . ")/i", "<span class=\"" . $_replace . "\">" . '${1}' . "</span>", $_subject);
		}
		else
		{
			return $_subject;
		}
	}

	public static function GetAssess($_key = null, $_default = null)
	{
		$valores = Request::Post($_key);

		if (is_array($valores))
		{
			asort($valores);

			foreach ($valores as $key => $value)
			{
				if ($value)
				{
					$v = self::Split($value, '_');
					$val[] = $v[1];
				}
			}

			return isset($val) ? $val : array();
		}
		else
		{
			return $_default;
		}
	}

	public static function ConcatArray($_array = null, $_concat = ',')
	{
		$val = null;

		if (is_array($_array))
		{
			foreach ($_array as $key => $value)
			{
				$val .= $value . $_concat;
			}
		}

		return ($val != null) ? substr($val, 0, -1) : $val;
	}

	public static function SuperGlob($_key)
	{
		if (isset($GLOBALS[$_key]))
		{
			return $GLOBALS[$_key];
		}
		else
		{
			return null;
		}
	}

	public static function ClearAccents($txt = null)
	{
		$encuentra = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
		$remplaza  = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";

		return strtr(utf8_decode($txt), utf8_decode($encuentra), $remplaza);
	}

	public static function ClearHTML($txt = null)
	{
		$buscar = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
		$reemplazar = array('>', '<', '\\1');
		$txt = preg_replace($buscar, $reemplazar, $txt);
		$txt = str_replace("> <", "><", $txt);

		return $txt;
	}

	public static function Equal($_value = null, $_match = "", $_return = null)
	{
		if ($_value == $_match)
		{
			if ($_return)
			{
				return $_return;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	public static function Lower($_string = null)
	{
		if (is_string($_string))
		{
			return strtolower(strtr($_string, "ÁÉÍÓÚÑ", "áéíóúñ"));
		}
		else
		{
			return $_string;
		}
	}

	public static function Upper($_string = null)
	{
		if (is_string($_string))
		{
			return strtoupper(strtr($_string, "áéíóúñ", "ÁÉÍÓÚÑ"));
		}
		else
		{
			return $_string;
		}
	}

	public static function SpecialChar($txt = null)
	{
		$txt = str_replace('<', '&lt;',$txt);
		$txt = str_replace('>', '&gt;',$txt);
		$txt = str_replace('"', '&quot;',$txt);

		return $txt;
	}

	public static function Split($_string = null, $_separator = ',')
	{
		return explode($_separator, $_string);
	}
}