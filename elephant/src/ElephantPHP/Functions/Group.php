<?php

/**
* @package	Group
* @version	1.2
* @author	DavidBeru
* @since	2013-08-29
* @see		2014-12-23
*/

class Group
{
	public function GroupArray($array, $groupkey)
	{
		if (count($array) > 0)
		{
			$keys = array_keys($array[0]);
			$removekey = array_search($groupkey, $keys);

			if ($removekey === false)
			{
				return array("Clave \"$groupkey\" no existe");
			}
			else
			{
				unset($keys[$removekey]);
				$groupcriteria = array();
				$return = array();

				foreach($array as $value)
				{
					$item = null;

					foreach ($keys as $key)
					{
						$item[$key] = $value[$key];
					}

					$busca = array_search($value[$groupkey], $groupcriteria);

					if ($busca === false)
					{
						$groupcriteria[] = $value[$groupkey];
						$return[] = array($groupkey => $value[$groupkey], "groupeddata" => array());
						$busca = count($return) - 1;
					}

					$return[$busca]["groupeddata"][] = $item;
				}

				return $return;
			}
		}
		else
		{
			return array();
		}
	}

	public function Key($array, $groupkey, $id, $valll)
	{
		if (count($array) > 0)
		{
			$keys = array_keys($array[0]);
			$removekey = array_search($groupkey, $keys);

			if ($removekey === false)
			{
				return array("Clave \"$groupkey\" no existe");
			}
			else
			{
				unset($keys[$removekey]);
				$groupcriteria = array();
				$return = array();

				foreach ($array as $qas => $value)
				{
					$item = null;

					foreach ($keys as $key => $val)
					{
						$item = $value[$valll];
						#$return[$value[$groupkey]]=array($groupkey=>$value[$groupkey]);
					}

					$busca = array_search($value[$groupkey], $groupcriteria);

					if ($busca === false)
					{
						$groupcriteria[] = $value[$groupkey];
						#$return[$value[$groupkey]]=array($groupkey=>$value[$groupkey]);
						$busca = count($return) - 1;
					}

					$return[$value[$groupkey]][$value[$id]]=$item;
				}

				return $return;
			}
		}
		else
		{
			return array();
		}
	}
}