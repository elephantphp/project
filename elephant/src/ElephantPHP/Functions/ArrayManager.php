<?php

/**
* @package	ArrayManager
* @version	1.1
* @author	DavidBeru
* @since	2013-07-26
* @see		2015-06-22
*/

class ArrayManager
{
	public static function Put($_array = null, $_key = null, $_value = null)
	{
		if (is_array($_array))
		{
			$is_array_key = is_array($_key);
			$is_string_key = is_string($_key);

			if ($is_array_key || $is_string_key || is_integer($_key))
			{
				if ($is_string_key && empty($_key))
				{
					return false;
				}
				else
				{
					if ($is_array_key)
					{
						$_array = array_merge($_array, $_key);
					}
					else
					{
						$split = String::Split($_key, '.');
						$count = count($split);

						if ($count <= 1)
						{
							if (is_array($_value))
							{
								$value = self::Get($_array, $_key, array());
								$value = is_array($value) ? $value : array();
								$_array[$_key] = array_merge($value, $_value);
							}
							else
							{
								$_array[$_key] = $_value;
							}
						}
						else
						{
							$value = $_value;

							for ($i = ($count - 1); $i > 0; $i--)
							{
								$array = array();
								$array[$split[$i]] = $value;
								$value = $array;
							}

							return self::Put($_array, $split[0], $value);
						}
					}

					return $_array;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public static function Get($_array = null, $_key = null, $_default = null)
	{
		if (is_array($_array))
		{
			if (is_string($_key))
			{
				$split = String::Split($_key, '.');
				$count = count($split);

				if ($count <= 1)
				{
					return array_key_exists($_key, $_array) ? $_array[$_key] : $_default;
				}
				else
				{
					$key = $split[0];

					if (array_key_exists($key, $_array))
					{
						$array = $_array;

						foreach ($split as $key => $value)
						{
							if (array_key_exists($value, $array))
							{
								$array = $array[$value];
							}
							else
							{
								$array = false;
								break;
							}
						}

						return ($array !== false) ? $array : $_default;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public static function Forget($_array = null, $_key = null)
	{
		if (is_array($_array))
		{
			if (is_string($_key) || is_integer($_key))
			{
				if (array_key_exists($_key, $_array))
				{
					unset($_array[$_key]);

					return $_array;
				}
				else
				{
					$val = $_array;
					$split = String::Split($_key, '.');
					$path = null;
					$elem = null;
					$seg = array();

					foreach ($split as $key => $value)
					{
						if (is_array($val))
						{
							if (array_key_exists($value, $val))
							{
								$add = Validator::Int($value) ? "[{$value}]" : "[\"{$value}\"]";
								$path .= $add;
								$seg[] = $add;
								$elem = $value;
							}

							$val = self::Get($val, $value, false);
						}
						else
						{
							break;
						}
					}

					if ($val === false)
					{
						return false;
					}
					else
					{
						if (Validator::Int($elem))
						{
							array_pop($seg);
							$path = null;

							foreach ($seg as $key => $value)
							{
								$path .= $value;
							}

							@eval("array_splice(\$_array{$path}, {$elem}, 1);");
						}
						else
						{
							@eval("unset(\$_array{$path});");
						}
					}

					return $_array;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public static function Avg($_array = null)
	{
		if (is_array($_array))
		{
			$val = 0;

			foreach ($_array as $key => $value)
			{
				$val += $value;
			}

			return @round($val / count($_array), 2);
		}
		else
		{
			return 0;
		}
	}

	public static function ArrayToString($_array = null, $_concat = null)
	{
		if (is_array($_array))
		{
			$string = null;
			$concat = is_string($_concat) ? $_concat : ',';

			foreach ($_array as $key => $value)
			{
				if (is_string($value))
				{
					$string .= "{$value}{$concat}";
				}
			}

			return is_null($string) ? null : substr($string, 0, -1);
		}
		else
		{
			return null;
		}
	}
}