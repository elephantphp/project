<?php

/**
* @package	Storagei
* @version	1.3
* @author	DavidBeru
* @since	2015-04-11
* @see		2015-08-22
*/

class Storagei
{
	private $storage = ELEPHANTPHP_STORAGE;
	private $directories = array(
		"/..",
		"cache",
		"logs",
		"media",
		"sessions",
		"simpledb");
	private $in = "media";
	private $elements = array();
	private $methods = array();

	public function __construct()
	{
		$this->storage = ELEPHANTPHP_STORAGE . "{$this->in}/";
	}

	public function __call($_method = null, $_arguments = null)
	{
		$this->methods[] = array("method" => $_method, "arguments" => $_arguments);

		return $this;
	}

	/**
	* Open a directory in search of files or directories
	*
	* @param	string	$_directory
	* @param	string	$_search		null | files | directories
	* @param	bool	$_recursive
	* @return	false | array
	*/

	private function OpenDirectory($_directory = null, $_search = null, $_recursive = false)
	{
		$path = $this->storage . $_directory;

		if (is_file($path))
		{
			return is_null($_search) ? "file" : false;
		}
		else
		{
			$directory = (substr($_directory, -1) == '/') ? $_directory : "{$_directory}/";
			$directory = (substr($directory, 0, 1) == '/') ? substr($directory, 1) : $directory;
			$path = $this->storage . $directory;

			if (file_exists($path))
			{
				$open = opendir($path);

				if ($open)
				{
					$exclude = array('.', "..");
					$elements = array();

					while (false !== ($element = readdir($open)))
					{
						if (!in_array($element, $exclude))
						{
							$route = "{$directory}{$element}";
							$path = $this->storage . $route;

							# Search directories and / or files
							if (is_null($_search))
							{
								if ($_recursive === true)
								{
									$elements[$element] = $this->OpenDirectory($route, $_search, $_recursive);
								}
								else
								{
									$elements[$element] = is_file($path) ? "file" : "directory";
								}
							}
							else
							{
								if ($_recursive === true)
								{
									if (is_file($path) && $_search == "files")
									{
										$this->elements[$route] = $element;
									}
									else if (is_dir("{$path}/"))
									{
										$open_directory = $this->OpenDirectory($route, $_search, $_recursive);

										if ($_search == "directories")
										{
											$this->elements[$route] = $element;
										}
									}
								}
								else
								{
									if (is_file($path) && $_search == "files")
									{
										$this->elements[$route] = $element;
									}
									else if (is_dir("{$path}/") && $_search == "directories")
									{
										$this->elements[$route] = $element;
									}
								}
							}
						}
					}

					closedir($open);

					return is_null($_search) ? $elements : $this->elements;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}

	public function Disk()
	{
		return $this;
	}

	public function In($_in = null)
	{
		$this->in = in_array($_in, $this->directories) ? $_in : $this->in;
		$this->storage = ELEPHANTPHP_STORAGE . "{$this->in}/";

		return $this;
	}

	public function Exists($_route = null)
	{
		return @file_exists($this->storage . $_route) ? true : false;
	}

	public function Get($_file = null)
	{
		$path = $this->storage . $_file;

		if ($this->Exists($_file) && is_file($path))
		{
			ob_start();
			@readfile($path);
			$data = @ob_get_clean();
			$unserialize = @unserialize($data);

			if ($unserialize !== false)
			{
				return $unserialize;
			}
			else
			{
				return $data ? $data : false;
			}
		}
		else
		{
			return false;
		}
	}

	public function Put($_file = null, $_content = null)
	{
		$make_directory = $this->MakeDirectory($_file);

		if ($make_directory)
		{
			if (is_string($_content))
			{
				$content = $_content;
			}
			else
			{
				$content = serialize($_content);

				if (!$content)
				{
					return false;
				}
			}

			return file_put_contents($this->storage . $_file, $content) ? true : false;
		}
		else
		{
			return $make_directory;
		}
	}

	public function Prepend()
	{
	}

	public function Append()
	{
	}

	public function Delete($_file = null)
	{
		$path = $this->storage . $_file;

		if ($this->Exists($_file))
		{
			$delete = @unlink($path) ? true : false;

			if (!$delete)
			{
				throw new Exception("Error, Permission denied");
			}

			return $delete;
		}
		else
		{
			return false;
		}
	}

	public function Copy($_old_file = null, $_new_file = null)
	{
		if ($this->Exists($_old_file))
		{
			return @copy($this->storage . $_old_file, $this->storage . $_new_file) ? true : false;
		}
		else
		{
			return false;
		}
	}

	public function Move($_old_file = null, $_new_file = null)
	{
		if ($this->Exists($_old_file))
		{
			return @rename($this->storage . $_old_file, $this->storage . $_new_file) ? true : false;
		}
		else
		{
			return false;
		}
	}

	public function Size($_file = null)
	{
		if ($this->Exists($_file))
		{
			return @filesize($this->storage . $_file);
		}
		else
		{
			return false;
		}
	}

	public function LastModified($_file = null)
	{
		if ($this->Exists($_file))
		{
			return @filemtime($this->storage . $_file);
		}
		else
		{
			return false;
		}
	}

	public function Elements($_directory = null)
	{
		return $this->OpenDirectory($_directory, null, false);
	}

	public function AllElements($_directory = null)
	{
		return $this->OpenDirectory($_directory, null, true);
	}

	public function Files($_directory = null)
	{
		return $this->OpenDirectory($_directory, "files", false);
	}

	public function AllFiles($_directory = null)
	{
		return $this->OpenDirectory($_directory, "files", true);
	}

	public function Directories($_directory = null)
	{
		return $this->OpenDirectory($_directory, "directories", false);
	}

	public function AllDirectories($_directory = null)
	{
		return $this->OpenDirectory($_directory, "directories", true);
	}

	public function MakeDirectory($_directory = null)
	{
		if ($this->Exists($_directory))
		{
			return true;
		}
		else
		{
			$path = $this->storage;
			$folders = String::Split($_directory, '/');
			$folder = $path;
			$loop = false;

			foreach ($folders as $value)
			{
				$val = String::Split($value, '.');

				if (count($val) == 1)
				{
					$folder .= $value . '/';

					if (file_exists($folder))
					{
						$loop = true;
					}
					else
					{
						$loop = @mkdir($folder) ? true : false;
					}

					#$loop = @mkdir($folder) ? true : false;
				}
			}

			if ($loop === true)
			{
				return true;
			}
			else
			{
				return $this->MakeDirectory($_directory);
			}
		}
	}

	public function DeleteDirectory($_directory = null)
	{
		if ($this->Exists($_directory))
		{
			$path = $this->storage . $_directory;

			if (is_dir($path))
			{
				$files = $this->AllFiles($_directory);
				$loop = array();

				if (is_array($files))
				{
					foreach ($files as $key => $value)
					{
						$loop[] = $this->Delete($key);
					}
				}

				if (in_array(false, $loop))
				{
					return $this->DeleteDirectory($_directory);
				}
				else
				{
					return @rmdir($path) ? true : false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}