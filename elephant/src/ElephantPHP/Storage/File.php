<?php

/**
* @package	File
* @version	2.9.3
* @author	DavidBeru
* @since	2014-11-05
* @see		2015-11-22
*/

class File
{
	private static $storage = ELEPHANTPHP_STORAGE;
	private static $dir = null;
	private static $type = null;
	private static $filename = null;
	private static $name = null;
	private static $ext = null;
	private static $content = null;
	private static $filemtime = 0;
	private static $size = 0;
	private static $perms = 0000;
	private static $dirs = array(
		"cache",
		"fms",
		"logs",
		"media",
		"sessions",
		"../elephant/assets");
	private static $mime_type = array(
		"pdf" => "application/pdf",
		"zip" => "application/zip",
		"png" => "image/png",
		"jpeg" => "image/jpeg",
		"jpg" => "image/jpg",
		"gif" => "image/gif",
		"mp3" => "audio/mpeg3",
		"wav" => "audio/wav",
		"ogg" => "audio/ogg",
		"avi" => "video/avi",
		"mp4" => "video/mp4",
		"csv" => "application/vnd.ms-excel",
		"txt" => "text/plain",
		"js" => "text/javascript");

	public function __tostring()
	{
		return self::$content;
	}

	public function Dir()
	{
		return self::$dir;
	}

	public function Type()
	{
		return self::$type;
	}

	public function Name()
	{
		return self::$name;
	}

	public function Ext()
	{
		return self::$ext;
	}

	public function Time()
	{
		return self::$filemtime;
	}

	public function Date()
	{
		return Date::Current(date("Y-m-d H:i:s", self::$filemtime));
	}

	public function Size()
	{
		return self::$size;
	}

	public function Perms()
	{
		return self::$perms;
	}

	public static function Storage($_dir = null)
	{
		$path = in_array($_dir, self::$dirs) ? $_dir . '/' : "media/";

		return self::$storage . $path;
	}

	public static function Has($_route = null, $_storage = null)
	{
		$path = self::Storage($_storage) . $_route;

		return (file_exists($path) && is_file($path)) ? true : false;
	}

	public static function Make($_route = null, $_content = null, $_storage = null)
	{
		Folder::Make($_route, $_storage);
		$make = @file_put_contents(self::Storage($_storage) . $_route, $_content);

		return ($make) ? true : false;
	}

	public static function Get($_route = null, $_storage = null)
	{
		$path = self::Storage($_storage) . $_route;

		if (file_exists($path) && is_file($path))
		{
			$file = PathInfo($_route);
			ob_start();
			@readfile($path);
			self::$content = ob_get_clean();
			self::$filemtime = filemtime($path);
			self::$size = filesize($path);
			self::$perms = substr(sprintf('%o', fileperms($path)), -4);
			self::$ext = self::GetExt($_route);
			self::$type = self::GetType(self::$ext);
			self::$name = $file["filename"];
			self::$dir = $file["dirname"];

			return new File();
		}
		else
		{
			return false;
		}
	}

	public static function GetType($_ext = null)
	{
		return array_key_exists($_ext, self::$mime_type) ? self::$mime_type[$_ext] : "text/{$_ext}";
	}

	public static function GetExt($_name = null)
	{
		return Fn::Lower(substr(strrchr($_name, '.'), 1));
	}

	public static function Upload($_key = null, $_dir = null, $_name = null)
	{
		if (is_null($_key))
		{
			return array(

				"status" => false,
				"error" => "falta el nombre del input file"

			);
		}
		else
		{
			if (is_array($_key))
			{
				if (isset($_key["key"]))
				{
					$key = $_key["key"];
				}
				else
				{
					return array(

						"status" => false,
						"error" => "key no existe en el arreglo"

					);
				}
			}
			else if (is_string($_key))
			{
				$key = $_key;
			}

			$file = Request::File($key);

			if (is_array($file))
			{
				Storage::MakeDirectory($_dir);
				$dir = is_null($_dir) ? null : ((substr($_dir, 0, -1) == '/') ? $_dir : $_dir . '/');
				$ext = $file["ext"];
				$name = $_name ? $_name : $file["name"];
				$filename = "{$name}.{$ext}";
				$route = "{$dir}{$filename}";

				if (@move_uploaded_file($file["tmp"], self::Storage("media") . "{$route}"))
				{
					return array(

						"status" => true,
						"error" => false,
						"type" => $file["type"],
						"ext" => $ext,
						"size" => $file["size"],
						"filename" => $filename,
						"name" => $name,
						"dir" => $dir,
						"route" => $route,
						"media" => URI::Media($route)

					);
				}
				else
				{
					return array(

						"status" => false,
						"error" => "no upload file",
						"type" => $file["type"],
						"ext" => $ext,
						"size" => $file["size"],
						"filename" => $filename,
						"name" => $name

					);
				}
			}
			else
			{
				return array(

					"status" => false,
					"error" => "no file selected"

				);
			}
		}
	}

	public static function Delete($_route = null, $_storage = null)
	{
		if (is_null($_route))
		{
			return null;
		}
		else
		{
			if (substr($_route, 0, 1) == '*')
			{
				$open = Folder::Open(null, $_storage);

				if (is_array($open))
				{
					foreach ($open as $key => $value)
					{
						if ($value == "file")
						{
							self::Delete($key, $_storage);
						}
					}

					return true;
				}
				else
				{
					return null;
				}
			}
			else
			{
				$path = self::Storage($_storage) . $_route;

				if (file_exists($path))
				{
					return @unlink($path);
				}
				else
				{
					return false;
				}
			}
		}
	}
}