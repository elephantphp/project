<?php

/**
* @package	FMS
* @version	1.8
* @author	DavidBeru
* @since	2014-11-17
* @see		2015-04-07
*/

class FMS
{
	private static $storage = ELEPHANTPHP_STORAGE;

	private static function Name($_route = null)
	{
		$split = Fn::Split($_route, '.');

		return (count($split) > 1) ? $_route : "{$_route}.fms";
	}

	public static function Make($_route = null, $_content = null, $_storage = "fms")
	{
		return File::Make(self::Name($_route), serialize($_content), $_storage);
	}

	public static function Get($_route = null, $_storage = "fms")
	{
		if (is_string($_route))
		{
			$file = File::Get(self::Name($_route), $_storage);

			return ($file) ? unserialize($file) : $file;
		}
		else if (is_array($_route))
		{
			$files = array();

			foreach ($_route as $key => $value)
			{
				$files[] = self::Get($value, $_storage);
			}

			return $files;
		}
		else
		{
			return false;
		}
	}

	public static function Has($_route = null, $_key = null, $_return_value = false, $_storage = "fms")
	{
		if (is_null($_route))
		{
			return null;
		}
		else if (is_null($_key))
		{
			return null;
		}
		else
		{
			$file = self::Get($_route, $_storage);

			if (is_array($file))
			{
				$split = Fn::Split($_key, '.');
				$key = (count($split) > 1) ? $split[0] : $_key;

				if (array_key_exists($key, $file))
				{
					$val = $file;

					foreach ($split as $key => $value)
					{
						$val = array_key_exists($value, $val) ? $val[$value] : false;
					}

					if ($_return_value === true)
					{
						return ($val !== false) ? $val : false;
					}
					else
					{
						return ($val !== false) ? true : false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return $file;
			}
		}
	}

	public static function Select($_route = null, $_data = array(), $_storage = "fms")
	{
		$open = Folder::Open($_route, $_storage);

		if ($open)
		{
			$files = array();
			$count = 0;
			$i = 0;
			$order = isset($_data["order"]) ? $_data["order"] : "asc";
			$limit = isset($_data["limit"]) ? $_data["limit"] : null;

			# Only Files
			$open = array_filter($open, function($_var)
			{
				return ($_var == "file");
			});

			# Get Keys
			$open = array_keys($open);

			# Order
			natcasesort($open);
			$open = ($order == "desc") ? array_reverse($open) : $open;

			# Count elements
			$elements = count($open);

			# Limit
			$split = Fn::Split($limit);

			if (isset($split[1]))
			{
				$limit_start = intval($split[0]);
				$limit_end = ($split[1] == "count") ? $elements : intval($split[1]);
			}
			else
			{
				$limit_start = 0;
				$limit_end = is_null($limit) ? $elements : intval($split[0]);
			}

			foreach ($open as $key => $value)
			{
				if (is_integer($limit_start) && is_integer($limit_end))
				{
					if ($i >= $limit_end)
					{
						break;
					}

					if ($count >= $limit_start)
					{
						$i++;
						$files[$count] = "{$_route}/{$value}";
					}

					$count++;
				}
			}

			return $files;
		}
		else
		{
			return $open;
		}
	}

	public static function Put($_route = null, $_key = null, $_value = null, $_storage = "fms")
	{
		$data = self::Get($_route, $_storage);

		if (is_array($data))
		{
			if (is_array($_key))
			{
				$data = array_merge($data, $_key);
			}
			else
			{
				if (is_array($_value))
				{
					$value = isset($data[$_key]) ? $data[$_key] : array();
					$value = is_array($value) ? $value : array();
					$data[$_key] = array_merge($value, $_value);
				}
				else
				{
					$data[$_key] = $_value;
				}
			}

			return self::Make($_route, $data, $_storage);
		}
		else
		{
			return $data;
		}
	}

	public static function Delete($_route = null, $_storage = "fms")
	{
		return File::Delete($_route, $_storage);
	}

	public static function Forget($_route = null, $_key = null, $_storage = "fms")
	{
		if (is_null($_key))
		{
			return null;
		}
		else
		{
			$has = self::Has($_route, $_key, false, $_storage);

			if ($has)
			{
				$data = self::Get($_route, $_storage);
				$val = $data;
				$split = Fn::Split($_key, '.');
				$path = null;
				$elem = null;
				$seg = array();

				foreach ($split as $key => $value)
				{
					if (isset($val[$value]))
					{
						$add = Validator::Int($value) ? "[{$value}]" : "[\"{$value}\"]";
						$path .= $add;
						$seg[] = $add;
						$elem = $value;
					}

					$val = isset($val[$value]) ? $val[$value] : null;
				}

				if (is_null($val))
				{
					return false;
				}
				else
				{
					if (Validator::Int($elem))
					{
						array_pop($seg);
						$path = null;

						foreach ($seg as $key => $value)
						{
							$path .= $value;
						}

						@eval("array_splice(\$data{$path}, {$elem}, 1);");
					}
					else
					{
						@eval("unset(\$data{$path});");
					}
				}

				return self::Put($_route, $data, null, $_storage);
			}
			else
			{
				return $has;
			}
		}
	}

	public static function Flush($_route = null, $_storage = "fms")
	{
		return self::Put($_route, array(), null, $_storage);
	}
}