<?php

/**
* @package	Storage
* @version	1.0
* @author	DavidBeru
* @since	2015-04-07
* @see		2015-04-18
*/

class Storage
{
	public static function __callStatic($_method = null, $_arguments = null)
	{
		$storagei = new Storagei();

		return call_user_func_array(array($storagei, $_method), $_arguments);
	}
}