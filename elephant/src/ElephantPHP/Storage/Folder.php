<?php

/**
* @package	Folder
* @version	2.6.1
* @author	DavidBeru
* @since	2014-11-05
* @see		2015-03-16
*/

class Folder
{
	private static $storage = ELEPHANTPHP_STORAGE;
	private static $dirs = array(
		"cache",
		"fms",
		"logs",
		"media",
		"sessions");

	private static function Storage($_dir = null)
	{
		$path = in_array($_dir, self::$dirs) ? $_dir . '/' : "media/";

		return self::$storage . $path;
	}

	public static function Make($_dir = null, $_storage = null, $_permissions = 0755)
	{
		if ($_dir)
		{
			$path = self::Storage($_storage);

			if (file_exists($path . $_dir))
			{
				return false;
			}
			else
			{
				$folders = Fn::Split($_dir, '/');
				$folder = $path;

				foreach ($folders as $value)
				{
					$val = Fn::Split($value, '.');

					if (count($val) == 1)
					{
						$folder .= $value . '/';
						@mkdir($folder, $_permissions);
					}
				}

				return true;
			}
		}
	}

	public static function Open($_route = null, $_storage = null, $_recursive = false)
	{
		$path = self::Storage($_storage) . $_route;

		if (is_file($path))
		{
			return "file";
		}
		else
		{
			$open = @opendir($path);

			if ($open)
			{
				$exclude = array('.', "..");
				$dirs = array();

				while (false !== ($dir = @readdir($open)))
				{
					if (!in_array($dir, $exclude))
					{
						if ($_recursive === true)
						{
							$path = "{$_route}/{$dir}";
							$dirs[$dir] = self::Open($path, $_recursive);
						}
						else
						{
							$path = self::Storage($_storage) . "{$_route}/{$dir}";
							$dirs[$dir] = is_file($path) ? "file" : "folder";
						}
					}
				}

				closedir($open);

				return $dirs;
			}
			else
			{
				return false;
			}
		}
	}

	public static function Delete($_route = null, $_storage = null)
	{
		if ($_route)
		{
			$path = self::Storage($_storage) . $_route;

			if (file_exists($path) && is_dir($path))
			{
				$open = self::Open($_route, $_storage);

				if (is_array($open))
				{
					foreach ($open as $key => $value)
					{
						if ($value == "file")
						{
							File::Delete("{$_route}/{$key}", $_storage);
						}
						else if ($value == "folder")
						{
							self::Delete("{$_route}/{$key}", $_storage);
						}
					}
				}

				return @rmdir($path);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return null;
		}
	}
}