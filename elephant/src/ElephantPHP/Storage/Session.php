<?php

/**
* @package	Session
* @version	4.0.2
* @author	DavidBeru
* @since	2013-07-21
* @see		2015-06-16
*/

class Session
{
	private static $storage = "sessions";
	private static $type = null;
	private static $folder = null;
	private static $sessid = null;

	private static function FolderToDate($_date = null)
	{
		if (strlen($_date) == 14)
		{
			$year = substr($_date, 0, 4);
			$month = substr($_date, 4, 2);
			$day = substr($_date, 6, 2);
			$hour = substr($_date, 8, 2);
			$minute = substr($_date, 10, 2);
			$second = substr($_date, 12, 2);

			return "{$year}-{$month}-{$day} {$hour}:{$minute}:{$second}";
		}
		else
		{
			return null;
		}
	}

	private static function Route()
	{
		if (Cookie::Has("SESSID"))
		{
			$value = Cookie::Get("SESSID");
			$split = String::Split($value, '_');

			if (count($split) > 1)
			{
				self::$type = $split[0];
				self::$folder = $split[1];
				self::$sessid = $split[2];

				return array(
					self::$type,
					self::$folder,
					self::$sessid);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return null;
		}
	}

	private static function Name()
	{
		return is_null(self::$sessid) ? str_replace('_', '/', Cookie::Get("SESSID")) . ".sess" : self::$type . '/' . self::$folder . '/' . self::$sessid . ".sess";
	}

	public static function Id()
	{
		$sess = self::Route();

		return is_array($sess) ? $sess[2] : $sess;
	}

	public static function Renew($_type = "general")
	{
		if (Cookie::Has("SESSID"))
		{
			$name = self::Name();
			$file = self::Get();

			if (is_array($file))
			{
				if ($_type === "user")
				{
					$type = "user";
					$length = ELEPHANTPHP_APP_USER_SESSION_LENGTH;
				}
				else
				{
					$type = "general";
					$length = ELEPHANTPHP_APP_SESSION_LENGTH;
				}

				$folder = date("YmdHis");
				$sessid = self::Id();
				$cookie = Cookie::Make("SESSID", "{$type}_{$folder}_{$sessid}", $length);

				if ($cookie === true)
				{
					$delete = Storage::In(self::$storage)->Delete($name);

					if ($delete === true)
					{
						$directory = self::$type . '/' . self::$folder; 
						$directory_files = Storage::In(self::$storage)->Files($directory);

						if (count($directory_files) === 0)
						{
							Storage::In(self::$storage)->DeleteDirectory($directory);
						}

						self::$type = $type;
						self::$folder = $folder;
						self::$sessid = $sessid;

						return Storage::In(self::$storage)->Put("{$type}/{$folder}/{$sessid}.sess", $file);
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public static function LastMovement()
	{
		$last_movement = self::Get("elephantphp.last-movement");

		if ($last_movement)
		{
			return Date::Diff($last_movement, date("Y-m-d H:i:s"), "seconds");
		}
		else
		{
			return 0;
		}
	}

	public static function Start()
	{
		$folder = Storage::In(self::$storage)->Directories("general");

		if (is_array($folder))
		{
			foreach ($folder as $key => $value)
			{
				$diff = Date::Diff(self::FolderToDate($value), date("Y-m-d H:i:s"), "seconds");

				if ($diff > ELEPHANTPHP_APP_SESSION_LENGTH)
				{
					Storage::In(self::$storage)->DeleteDirectory($key);
				}
			}
		}

		$folder = Storage::In(self::$storage)->Directories("user");

		if (is_array($folder))
		{
			foreach ($folder as $key => $value)
			{
				$diff = Date::Diff(self::FolderToDate($value), date("Y-m-d H:i:s"), "seconds");

				if ($diff > ELEPHANTPHP_APP_USER_SESSION_LENGTH)
				{
					Storage::In(self::$storage)->DeleteDirectory($key);
				}
			}
		}

		if (Cookie::Has("SESSID"))
		{
			$sessid = self::Id();
			$type = self::$type;
			$folder = self::$folder;

			if (Storage::In(self::$storage)->Exists("{$type}/{$folder}/{$sessid}.sess"))
			{
				return true;
			}
			else
			{
				return Storage::In(self::$storage)->Put("{$type}/{$folder}/{$sessid}.sess", array());
			}
		}
		else
		{
			$type = "general";
			$folder = date("YmdHis");
			$sessid = Hash::Make(Hash::Generate(40));
			$cookie = Cookie::Make("SESSID", "{$type}_{$folder}_{$sessid}", ELEPHANTPHP_APP_SESSION_LENGTH);

			if ($cookie === true && Cookie::Has("SESSID"))
			{
				return Storage::In(self::$storage)->Put("{$type}/{$folder}/{$sessid}.sess", array());
			}
		}
	}

	public static function Put($_key = null, $_value = null)
	{
		$data = ArrayManager::Put(self::Get(), $_key, $_value);

		if (is_array($data))
		{
			return Storage::In(self::$storage)->Put(self::Name(), $data);
		}
		else
		{
			return false;
		}
	}

	public static function Has($_key = null)
	{
		$array_get = ArrayManager::Get(self::Get(), $_key, false);

		return ($array_get !== false) ? true : false;
	}

	public static function Get($_key = null, $_default = null)
	{
		$name = self::Name();

		if (is_null($_key))
		{
			return Storage::In(self::$storage)->Get($name);
		}
		else
		{
			return ArrayManager::Get(self::Get(), $_key, $_default);
		}
	}

	public static function Forget($_key = null)
	{
		$array = ArrayManager::Forget(self::Get(), $_key);

		if (is_array($array))
		{
			return Storage::In(self::$storage)->Put(self::Name(), $array);
		}
		else
		{
			return false;
		}
	}

	public static function Flush()
	{
		return Storage::In(self::$storage)->Put(self::Name(), array());
	}
}