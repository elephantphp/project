<?php

/**
* @package	App
* @version	1.0
* @author	DavidBeru
* @since	2015-10-06
* @see		2015-10-06
*/

class App
{
	public static function Config($_file = null, $_key = null)
	{
		return ElephantPHP::GetAppConfig($_file, $_key);
	}

	public static function Databases()
	{
		return self::Config("databases");
	}

	public static function FTP()
	{
		return self::Config("ftp");
	}

	public static function Libraries()
	{
		return self::Config("libraries");
	}

	public static function Routes()
	{
		return self::Config("routes");
	}

	public static function Secrets($_key = null)
	{
		return self::Config("secrets", $_key);
	}

	public static function Views($_key = null)
	{
		return self::Config("views", $_key);
	}
}