<?php

/**
* @package	SimpleDBi
* @version	1.1
* @author	DavidBeru
* @since	2015-04-08
* @see		2015-04-29
*/

class SimpleDBi
{
	private $storage = "simpledb";
	private $ext = "sdb";
	private $database = null;
	private $collection = null;
	private $path = null;
	private $id = null;
	private $error = null;
	private $result = null;
	private $limit = null;
	private $count = 0;

	public function __construct($_database = null)
	{
		$this->database = is_string($_database) ? $_database : null;
	}

	public function __tostring()
	{
		return $this->result;
	}

	private function Initialize()
	{
		$this->StoragePut("{$this->database}/info.{$this->ext}", null);
		$this->StoragePut("{$this->database}/sequences.{$this->ext}", array(

			"{$this->collection}" => 1

		));
	}

	private function StoragePut($_file = null, $_content = null)
	{
		return Storage::In($this->storage)->Put($_file, serialize($_content));
	}

	private function StorageGet($_file = null)
	{
		$get = Storage::In($this->storage)->Get($_file);

		return ($get) ? unserialize($get) : $get;
	}

	private function StorageMakeDirectory($_directory = null)
	{
		return Storage::In($this->storage)->MakeDirectory($_directory);
	}

	private function StorageExists($_route = null)
	{
		return Storage::In($this->storage)->Exists($_route);
	}

	private function SetNextSequence()
	{
		return $this->Put("{$this->database}/sequences.{$this->ext}", array(

			"{$this->collection}" => ($this->GetNextSequence() + 1)

		));
	}

	private function Put($_file = null, $_key = null, $_value = null)
	{
		$data = $this->StorageGet($_file);

		if (is_array($data))
		{
			if (is_array($_key))
			{
				$data = array_merge($data, $_key);
			}
			else
			{
				if (is_array($_value))
				{
					$value = isset($data[$_key]) ? $data[$_key] : array();
					$value = is_array($value) ? $value : array();
					$data[$_key] = array_merge($value, $_value);
				}
				else
				{
					$data[$_key] = $_value;
				}
			}

			return $this->StoragePut($_file, $data);
		}
		else
		{
			return $data;
		}
	}

	private function GetDocument($_document = null)
	{
		return $this->StorageGet("{$this->database}/{$this->collection}/{$_document}");
	}

	private function Criteria($_criteria = null, $_document = null)
	{
		if (is_null($_criteria))
		{
			return true;
		}
		else
		{
			if (is_array($_criteria) && is_array($_document))
			{
				$keys = array_keys($_criteria);
				$values = array_values($_criteria);
				$diff = array();
				$criteria = true;

				foreach ($keys as $key => $value)
				{
					if (array_key_exists($value, $_document))
					{
						if ($_document[$value] == $values[$key])
						{
							$diff[] = true;
						}
						else
						{
							$diff[] = "The value {$values[$key]} no exists in the document";
							$criteria = false;
						}
					}
					else
					{
						$diff[] = "The field {$value} no exists in the document";
						$criteria = false;
					}
				}

				return $criteria ? $criteria : $diff;
			}
			else
			{
				return false;
			}
		}
	}

	/**
	* @category Public
	*/

	public function Collection($_collection = null)
	{
		$split = String::Split($_collection, '.');
		$count = count($split);

		if ($count == 1)
		{
			$this->collection = $_collection;
		}
		else if ($count == 2)
		{
			$this->database = $split[0];
			$this->collection = $split[1];
		}

		$this->path = "{$this->database}/{$this->collection}";

		return $this;
	}

	public function GetNextSequence()
	{
		$get = $this->StorageGet("{$this->database}/sequences.{$this->ext}");
		$seq = isset($get[$this->collection]) ? $get[$this->collection] : 1;

		return $seq;
	}

	public function Limit($_cursor = null)
	{
		$this->limit = $_cursor;

		return $this;
	}

	public function Result()
	{
		return $this->result;
	}

	public function Count()
	{
		return $this->count;
	}

	public function CreateIndex($_fields = null, $_indexes = null)
	{
	}

	public function RemoveIndex()
	{
	}

	public function Drop()
	{
	}

	/**
	* @category CRUD
	*/

	public function Insert($_document = null)
	{
		if (is_array($_document))
		{
			if (!$this->StorageExists("{$this->database}/info.{$this->ext}"))
			{
				$this->Initialize();
			}

			$sequence = $this->GetNextSequence();
			$document = array_merge(array(

				"_id" => Hash::Make(),
				"_seq" => $sequence

			), $_document);
			$insert = $this->StoragePut("{$this->path}/{$sequence}.{$this->ext}", $document);

			if ($insert === true)
			{
				$this->SetNextSequence();

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function Find($_criteria = null, $_projection = null)
	{
		$files = Storage::In($this->storage)->Files("{$this->database}/{$this->collection}/");

		if (is_array($files))
		{
			natcasesort($files);
			$documents = array();
			$count = 0;

			for ($i = 0; $i < count($files); $i++)
			{
				if ($this->limit === $i)
				{
					break;
				}
				else
				{
					$document = $this->GetDocument($files[$i]);
					$criteria = $this->Criteria($_criteria, $document);

					if ($criteria === true)
					{
						$documents[] = $document;
						$count++;
					}
				}
			}

			$this->result = $documents;
			$this->count = $count;
		}

		return $this;
	}

	public function Update($_criteria = null, $_action = null, $_option = null)
	{
	}

	public function Remove($_criteria = null)
	{
	}
}