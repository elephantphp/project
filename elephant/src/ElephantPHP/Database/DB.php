<?php

/**
* @package	DB
* @version	3.3
* @author	DavidBeru
* @since	2014-02-12
* @see		2015-06-14
*/

class DB
{
	public static function Connect($_db = null)
	{
		$db = new DBi();
		$db->Connect($_db);

		return $db;
	}

	public static function UseDataBase($_db = null)
	{
		$db = new DBi();
		$db->UseDataBase($_db);

		return $db;
	}

	public static function Query($_sql = null, $_db = null)
	{
		$db = new DBi();
		$db->Query($_sql, $_db);

		return $db;
	}

	public static function Select($_sql = null, $_db = null)
	{
		$db = new DBi();
		$db->Select($_sql, $_db);

		return $db;
	}

	public static function Update($_sql = null, $_table = null, $_db = null)
	{
		$db = new DBi();
		$db->Update($_sql, $_table, $_db);

		return $db;
	}

	public static function UpdateNoNull($_sql = null, $_table = null, $_db = null)
	{
		$db = new DBi();
		$db->UpdateNoNull($_sql, $_table, $_db);

		return $db;
	}

	public static function Insert($_sql = null, $_table = null, $_db = null)
	{
		$db = new DBi();
		$db->Insert($_sql, $_table, $_db);

		return $db;
	}

	public static function Delete($_sql = null, $_db = null)
	{
		$db = new DBi();
		$db->Delete($_sql, $_db);

		return $db;
	}
}