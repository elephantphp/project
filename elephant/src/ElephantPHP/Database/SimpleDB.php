<?php

/**
* @package	SimpleDB
* @version	1.0
* @author	DavidBeru
* @since	2015-04-08
* @see		2015-04-18
*/

class SimpleDB
{
	private static $database = null;
	private static $storage = "simpledb";

	public static function ShowDataBases()
	{
		return Storage::In(self::$storage)->Directories();
	}

	public static function UseDataBase($_database = null)
	{
		if (Storage::In(self::$storage)->Exists($_database))
		{
			self::$database = is_string($_database) ? $_database : null;

			return true;
		}
		else
		{
			return false;
		}
	}

	public static function ShowCollections($_database = null)
	{
		$database = is_string($_database) ? $_database : self::$database;

		return Storage::In(self::$storage)->Directories($database);
	}

	public static function Collection($_collection = null)
	{
		$simple_db = new SimpleDBi(self::$database);
		$simple_db->Collection($_collection);

		return $simple_db;
	}
}