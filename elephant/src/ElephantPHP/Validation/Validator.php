<?php

/**
* @package	Validator
* @version	1.2.4
* @author	DavidBeru
* @since	2013-07-26
* @see		2015-04-13
*/

class Validator
{
	public static function Float($_input = null)
	{
		return is_float($_input) ? true : false;
	}

	public static function Numeric($_input = null)
	{
		return is_numeric($_input) ? true : false;
	}

	public static function Int($_input = null)
	{
		if (self::Numeric($_input))
		{
			$_input = (int) $_input;

			return is_int($_input) ? true : false;
		}
		else
		{
			return false;
		}
	}

	public static function Bool($_input = null)
	{
		return filter_var($_input, FILTER_VALIDATE_BOOLEAN) ? true : false;
	}

	public static function Date($_input = null)
	{
		if (preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $_input))
		{
			$date = explode('-', $_input);

			return checkdate($date[1], $date[2], $date[0]) ? true : false;
		}
		else
		{
			return false;
		}
	}

	public static function Time($_input = null)
	{
		return preg_match('/^\d{2}\:\d{2}\:\d{2}$/', $_input) ? true : false;
	}

	public static function DateTime($_input = null)
	{
		return preg_match('/^\d{4}\-\d{2}\-\d{2}\ \d{2}\:\d{2}\:\d{2}$/', $_input) ? true : false;
	}

	public static function Email($_input = null)
	{
		return filter_var($_input, FILTER_VALIDATE_EMAIL) ? true : false;
	}

	public static function CURP($_curp = null)
	{
		# Mask: AAAA######AAAAAA## - 18
		# Mask: AAAA######AAAAAAA# - 18
		return preg_match('/^([A-Z]{4})([0-9]{6})([A-Z]{6})([0-9]{1}|[A-Z{1}])([0-9]{1})$/i', $_curp) ? true : false;
	}

	public static function CP($_cp = null)
	{
		# Mask: #####
		return preg_match('/^[0-9]{5}$/i', $_cp) ? true : false;
	}

	public static function Phone($_phone = null)
	{
		# Mask: 9999999999
		return preg_match('/^[0-9]{10}$/i', $_phone) ? true : false;
	}

	public static function Name($_input = null)
	{
		return preg_match('/^[A-Z üÜáéíóúÁÉÍÓÚñÑ]{2,80}$/i', $_input) ? true : false;
	}

	public static function Location($_input = null)
	{
		return preg_match('/^[a-záÁéÉíÍóÓúÚ, ]{1,100}$/i', $_input) ? true : false;
	}

	public static function UserName($_input = null, $delimiter = 15)
	{
		return preg_match('/^[a-z\d_.]{4,' . $delimiter . '}$/i', $_input) ? true : false;
	}

	public static function Password($_input = null)
	{
		return preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $_input) ? true : false;
	}

	public static function IP($_input = null)
	{
		return filter_var($_input, FILTER_VALIDATE_IP) ? true : false;
	}

	public static function URL($_input = null)
	{
		return filter_var($_input, FILTER_VALIDATE_URL) ? true : false;
	}

	public static function Hexa($_input = null)
	{
		return preg_match('/^#(?:(?:[a-f\d]{3}){1,2})$/i', $_input) ? true : false;
	}

	public static function Assess($_value = null)
	{
		$values = array();

		foreach ($_value as $key => $value)
		{
			$index = explode('_', $value);
			$values[] = $index[0];
		}

		$a = array_count_values($values);

		foreach ($a as $key => $value)
		{
			if ($value > 1)
			{
				$status = false;
			}
		}

		return isset($status) ? $status : true;
	}
}