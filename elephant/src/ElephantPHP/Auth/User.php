<?php

/**
* @package	User
* @version	2.6.2
* @author	DavidBeru
* @since	2014-07-14
* @see		2015-04-27
*/

class User
{
	private static $user_id = null;
	private static $data = array();

	private static function GID($_input = array())
	{
		$sess = Session::Get("elephantphp.user_gid");

		if (is_array($_input))
		{
			if (is_array($sess))
			{
				$return = false;

				foreach ($sess as $key => $value)
				{
					if (in_array($value, $_input))
					{
						$return = true;
					}
				}
			}
			else
			{
				$return = in_array($sess, $_input) ? true : false;
			}
		}
		else
		{
			if (is_array($sess))
			{
				$return = in_array($_input, $sess) ? true : false;
			}
			else
			{
				$return = ($_input == $sess) ? true : false;
			}
		}

		if ($return === true)
		{
			return array($sess);
		}
		else
		{
			return "No tienes permisos";
		}
	}

	public static function Id($_value = null)
	{
		if (is_null($_value))
		{
			$user_id = Session::Get("elephantphp.user_id");

			return ($user_id) ? $user_id : self::$user_id;
		}
		else
		{
			if (ELEPHANTPHP_APP_COOKIE_DOMAIN === '.' . URI::Domain())
			{
				Session::Renew("user");
			}

			self::$user_id = $_value;

			return Session::Put("elephantphp.user_id", $_value);
		}
	}

	public static function SetGID($_input = 0)
	{
		return Session::Put("elephantphp.user_gid", $_input);
	}

	public static function Data($_key = null, $_value = null)
	{
		if ($_key && $_value)
		{
			$data = self::Data();

			if (is_array($data))
			{
				self::$data = $data;
			}

			self::$data[$_key] = $_value;

			return Session::Put("elephantphp.user_data", self::$data);
		}
		else if ($_key)
		{
			return Session::Get("elephantphp.user_data.{$_key}");
		}
		else
		{
			return Session::Get("elephantphp.user_data");
		}
	}

	public static function Auth($_input = null, $_gid = null)
	{
		if (is_callable($_input))
		{
			if (Session::Has("elephantphp.user_id"))
			{
				$user_id = self::Id();

				if (is_null($_gid))
				{
					return call_user_func($_input, $user_id, $_gid);
				}
				else
				{
					$gid = self::GID($_gid);

					if (is_string($gid))
					{
						Response::Error(800, $gid);
					}
					else
					{
						return call_user_func($_input, $user_id, $gid[0]);
					}
				}
			}
			else
			{
				if (Request::AJAX())
				{
					Response::Send("Please sign in again");
				}
				else
				{
					Session::Put("elephantphp.login-and-back-to", URI::Path());

					return Redirect::Auth();
				}
			}
		}
		else
		{
			return Session::Has("elephantphp.user_id");
		}
	}

	public static function BackTo($_forget = false)
	{
		if ($_forget === true)
		{
			return Session::Forget("elephantphp.login-and-back-to");
		}
		else
		{
			if (Session::Has("elephantphp.login-and-back-to"))
			{
				return Session::Get("elephantphp.login-and-back-to");
			}
			else
			{
				return null;
			}
		}
	}

	public static function Login()
	{
	}

	public static function Logout()
	{
		self::$user_id = null;
		Session::Forget("elephantphp.user_id");
		Session::Forget("elephantphp.user_gid");
		Session::Forget("elephantphp.user_data");
		Session::Renew("general");
	}

	public static function Shard($_value = 0, $_max = 0)
	{
		$split = String::Split($_value, ',');

		if (count($split) > 1)
		{
			$value = intval($split[0]);
			$value = Validator::Int($value) ? intval($value) : 1;
			$max = $split[1];
			$max = Validator::Int($max) ? $max : 1;
		}
		else
		{
			$value = intval($_value);
			$max = Validator::Int($_max) ? $_max : 1;
		}

		$max = ($max != 0) ? $max : 1;

		return ($value % $max) + 1;
	}
}