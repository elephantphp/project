<?php

/**
* @package	Date
* @version	3.6.2
* @author	DavidBeru
* @since	2013-07-21
* @see		2015-02-07
*/

class Date
{
	private static $type = null;
	private static $output = null;
	private static $string = null;
	private static $unix = 0;
	private static $regex_date = '/@\{date\}/i';
	private static $regex_date_string = '/@\{date.string\}/i';
	private static $regex_datetime = '/@\{date.datetime\}/i';
	private static $regex_datetime_string = '/@\{date.datetime.string\}/i';

	public function __tostring()
	{
		return self::$output;
	}

	public static function OutPut()
	{
		return self::$output;
	}

	public static function ToString()
	{
		return self::$string;
	}

	public static function ToUnix()
	{
		return self::$unix;
	}

	public static function Add($_a = 0, $_b = 0, $_c = 0, $_d = 0, $_e = 0, $_f = 0)
	{
		$date = new DateTime(self::$output);

		switch (self::$type)
		{
			case "current":
				$date->add(new DateInterval("P{$_a}Y{$_b}M{$_c}D"));
				self::$output = $date->format("Y-m-d");
				break;
			case "time":
				$date->add(new DateInterval("PT{$_a}H{$_b}M{$_c}S"));
				self::$output = $date->format("H:i:s");
				break;
			case "datetime":
				$date->add(new DateInterval("P{$_a}Y{$_b}M{$_c}DT{$_d}H{$_e}M{$_f}S"));
				self::$output = $date->format("Y-m-d H:i:s");
				break;
		}

		return new Date();
	}

	public static function Year()
	{
		return date('Y');
	}

	public static function Month()
	{
		return date('m');
	}

	public static function Day()
	{
		return date('d');
	}

	public static function Hour()
	{
		return date('H');
	}

	public static function Minutes()
	{
		return date('i');
	}

	public static function Seconds()
	{
		return date('s');
	}

	public static function YMD($_f = '-')
	{
		return date("Y{$_f}m{$_f}d");
	}

	public static function HMS($_f = ':')
	{
		return date("H{$_f}i{$_f}s");
	}

	public static function Current($_date = null)
	{
		self::$type = "current";
		self::$output = is_null($_date) ? date("Y-m-d") : $_date;

		if (strlen(self::$output) == 10)
		{
			list($year, $month, $day) = explode('-', self::$output);
		}
		else
		{
			list($date, $time) = explode(' ', self::$output);
			list($year, $month, $day) = explode('-', $date);
		}

		self::$unix = mktime(0, 0, 0, $month, $day, $year);
		self::$string = date("l, jS \of F Y", self::$unix);

		return new Date();
	}

	public static function Time($_time = null)
	{
		self::$type = "time";
		self::$output = is_null($_time) ? date("H:i:s") : $_time;
		list($hour, $minute, $second) = explode(':', self::$output);
		self::$unix = mktime($hour, $minute, $second);
		self::$string = date("h:i A");

		return new Date();
	}

	public static function DateTime($_datetime = null)
	{
		self::$type = "datetime";
		self::$output = is_null($_datetime) ? date("Y-m-d H:i:s") : $_datetime;

		if (Validator::DateTime(self::$output))
		{
			list($date, $time) = explode(' ', self::$output);
			list($year, $month, $day) = explode('-', $date);
			list($hour, $minute, $second) = explode(':', $time);
			self::$unix = mktime($hour, $minute, $second, $month, $day, $year);
			self::$string = date("l, jS \of F Y h:i A", self::$unix);
		}

		return new Date();
	}

	public static function Now($_datetime = null)
	{
		return self::DateTime($_datetime);
	}

	public static function Diff($_datetime1 = null, $_datetime2 = null, $_format = 'H')
	{
		try
		{
			$datetime1 = new DateTime($_datetime1);
			$datetime2 = new DateTime($_datetime2);
			$interval = $datetime1->diff($datetime2);

			switch ($_format)
			{
				case "minutes":
					$years = ($interval->format("%y%") * 525600);
					$months = ($interval->format("%m%") * 43200);
					$days = ($interval->format("%d%") * 1440);
					$hours = ($interval->format("%h%") * 60);
					$minutes = $interval->format("%i%");
					$diff = $years + $months + $days + $hours + $minutes;
					break;
				case "seconds":
					$years = ($interval->format("%y%") * 31536000);
					$months = ($interval->format("%m%") * 2592000);
					$days = ($interval->format("%d%") * 86400);
					$hours = ($interval->format("%h%") * 3600);
					$minutes = ($interval->format("%i%") * 60);
					$seconds = $interval->format("%s%");
					$diff = $years + $months + $days + $hours + $minutes + $seconds;
					break;
				default:
					$diff = $interval->format("%{$_format}%");
					break;
			}

			return $diff;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	public static function TimeDiff($_date = null)
	{
		return date("H:i:s", strtotime("00:00:00") + strtotime(self::Current()) - strtotime($_date));	
	}

	public static function Age($_date = null)
	{
		$d = date('j');
		$m = date('n');
		$y = date('Y');
		$d_nac = substr($_date, 8, 2);
		$m_nac = substr($_date, 5, 2);
		$y_nac = substr($_date, 0, 4);

		if ($m_nac > $m)
		{
			$age = $y - $y_nac - 1;
		}
		else
		{
			if ($m == $m_nac && $d_nac > $d)
			{
				$age = $y - $y_nac - 1;
			}
			else
			{
				$age = $y - $y_nac;
			}
		}

		return $age;
	}

	public static function DayAgo($_date_start = null, $_date_end = null)
	{
		$days = (strtotime($_date_start) - strtotime($_date_end)) / 86400;
		$days = floor(abs($days));

		return $days;
	}

	public static function Interpret($_string = null)
	{
		$string = preg_replace_callback(self::$regex_date, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_date, $value))
				{
					$val = self::Current();
				}
			}

			return $val;

		}, $_string);

		$string = preg_replace_callback(self::$regex_date_string, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_date_string, $value))
				{
					$val = self::Current()->ToString();
				}
			}

			return $val;

		}, $string);

		$string = preg_replace_callback(self::$regex_datetime, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_datetime, $value))
				{
					$val = self::DateTime();
				}
			}

			return $val;

		}, $string);

		$string = preg_replace_callback(self::$regex_datetime_string, function($matches)
		{
			$val = null;

			foreach ($matches as $key => $value)
			{
				if (preg_match(self::$regex_datetime_string, $value))
				{
					$val = self::DateTime()->ToString();
				}
			}

			return $val;

		}, $string);

		return $string;
	}
}