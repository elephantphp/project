<?php

/**
* @package	Mail
* @version	2.1
* @author	DavidBeru
* @since	2013-11-02
* @see		2015-02-20
*/

class Mail
{
	public static function Send($_title = null, $_content = null, $_email = null, $_from = "Webmaster <no-reply@webmaster.com>")
	{
		if (empty($_title))
		{
			return 701;
		}
		else if (empty($_content))
		{
			return 702;
		}
		else if (empty($_email))
		{
			return 703;
		}
		else
		{
			if (is_array($_email))
			{
				$return = array();

				foreach ($_email as $key => $value)
				{
					$return[$value] = self::Send(
						$_title,
						$_content,
						$value,
						$_from);
				}

				return $return;
			}
			else
			{
				if (Validator::Email($_email))
				{
					$headers = "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=utf-8\r\n";
					$headers .= "From: {$_from}\r\n";
					$mail = Mail(
						$_email,
						$_title,
						$_content,
						$headers);

					if ($mail)
					{
						return true;
					}
					else
					{
						return 705;
					}
				}
				else
				{
					return 704;
				}
			}
		}
	}
}