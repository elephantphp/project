<?php

/**
* @package	FTP
* @version	0.1.2
* @author	DavidBeru
* @since	2013-07-23
* @see		2014-11-22
*/

class FTP
{
	public static function UploadFile($route = null, $save = null, $conn = null)
	{
		if ($route && $save)
		{
			$connection = ftp_connect($conn['server']);
			$login = ftp_login($connection,$conn['user'],$conn['pass']);

			try
			{
				ftp_put($connection, $save, $route, FTP_ASCII);

				return true;
			}
			catch(Exception $e)
			{
				return false;
			}

			ftp_close($connection);
		}
	}

	public static function NewDir($dir = null, $conn = null)
	{
		$connection = ftp_connect($conn['server']);
		$login = ftp_login($connection,$conn['user'], $conn['pass']);

		if (@ftp_mkdir($connection, $dir))
		{
			return true;
		}
		else
		{
			return false;
		}

		ftp_close($connection);
	}

	public static function Rename($old = null, $new = null, $conn = null)
	{
		$connection = ftp_connect($conn['server']);
		$login = ftp_login($connection,$conn['user'],$conn['pass']);

		if (@ftp_rename($connection,$old,$new))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function OpenDir($dir = ".", $conn = null)
	{
		$connection = ftp_connect($conn['server']);
		$login = ftp_login($connection,$conn['user'],$conn['pass']);

		$buff = ftp_nlist($connection,$dir);
		$pos = 0;

		foreach ($buff as $key => $value)
		{
			if ($value != '.' || $value != '..')
			{
				$return[$pos] = $value;
				$pos++;
			}
		}

		return $return;
	}
}