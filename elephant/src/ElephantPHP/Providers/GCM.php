<?php

/**
* @package	Google Cloud Messaging
* @version	1.6
* @author	Gabriel
*			DavidBeru
* @since	2015-02-12
* @see		2015-10-06
*/

class GCM
{
	private static $url = "https://android.googleapis.com/gcm/send";

	public static function Send($_data = null, $_ids = null, $_options = null)
	{
		#$config = ElephantPHP::GetAppConfig("config");
		#$gcm_key = isset($config["gcm_key"]) ? $config["gcm_key"] : null;
		$gcm_key = App::Secrets("gcm_key");

		if (empty($gcm_key))
		{
			return "error, api key undefined";
		}
		else if (!is_string($gcm_key))
		{
			return "error, api key should be a string";
		}
		else if (!is_array($_data))
		{
			return "error, data should be an array";
		}
		else if (!is_array($_ids))
		{
			return "error, ids should be an array";
		}
		else
		{
			$content = array(

				"registration_ids" => $_ids,
				"data" => $_data,
				"timeToLive" => 86400

			);
			$data = is_array($_options) ? array_merge($content, $_options) : $content;
			$curl = Request::CURL(self::$url, array(

				"Authorization: key=" . $gcm_key,
				"Content-Type: application/json"

			), $data);

			return ($curl) ? $curl : false;
		}
	}
}