<?php

/**
* @package	WebSocket - Based on Socket
* @version	1.1
* @author	PHP WebSocket Server
*			DavidBeru
* @since	2015-08-07
* @see		2015-08-12
*/

class WebSocket
{
	private $server = null;
	private $port = null;
	private $socket = null;

	public function __construct($_server = null, $_port = 8080)
	{
		if ($_server && $_port)
		{
			$this->server = $_server;
			$this->port = $_port;

			$this->socket = new Socket();
			$this->socket->bind("message", "WebSocket::wsOnMessage");
			$this->socket->bind("open", "WebSocket::wsOnOpen");
			$this->socket->bind("close", "WebSocket::wsOnClose");
		}
	}

	public function Start()
	{
		set_time_limit(0);

		return $this->socket->wsStartServer($this->server, $this->port);
	}

	# When a client sends data to the server
	public static function wsOnMessage($_server, $clientID, $message, $messageLength, $binary) 
	{
		$ip = long2ip($_server->wsClients[$clientID][6]);

		# Check if message length is 0
		if ($messageLength == 0)
		{
			$_server->wsClose($clientID);

			return;
		}
		else
		{
			# The speaker is the only person in the room. Don't let them feel lonely.
			if (sizeof($_server->wsClients) == 1)
			{
				$_server->wsSend($clientID, "");
			}
			else
			{
				# Send the message to everyone but the person who said it
				foreach ($_server->wsClients as $id => $client)
				{
					$_server->wsSend($id, $message);
				}
			}
		}
	}

	# When a client connects
	public static function wsOnOpen($_server, $clientID)
	{
		$ip = long2ip($_server->wsClients[$clientID][6]);
		$_server->log("");

		# Send a join notice to everyone but the person who joined
		foreach ($_server->wsClients as $id => $client)
		{
			if ($id != $clientID)
			{
				$_server->wsSend($id, "");
			}
		}
	}

	# When a client closes or lost connection
	public static function wsOnClose($_server, $clientID, $status)
	{
		$ip = long2ip($_server->wsClients[$clientID][6]);
		$_server->log("");

		foreach ($_server->wsClients as $id => $client)
		{
			$_server->wsSend($id, "");
		}
	}
}