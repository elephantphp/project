<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ElephantPHP</title>
	<link rel="stylesheet" href="<?php echo URI::ElephantPHPAssets("css/style.css"); ?>">
</head>
<body class="elephantphp">
	<header>
		<nav>
			<?php echo View::Make("@{path}.elephant.views.header"); ?>
		</nav>
	</header>
	<section>
		<div id="logo"></div>
		<h1>Hello Elephant!</h1>
	</section>
</body>
</html>