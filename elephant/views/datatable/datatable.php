<div class="datatable">
	<?php echo Form::Hidden("datatable-url-{$name}", URI::Home("elephantphp/controls/datatable")); ?>
	<div class="header">
		<h4><?php echo @$title; ?></h4>
	</div>
	<div class="controls">
		<div>
			<div>
				Buscar
			</div>
			<div>
				<?php echo Form::Text($name, "", array(
						"data-fw-control-type" => "datatable-search",
						"data-fw-control" => $control,
						"data-fw-control-name" => $name,
						"placeholder" => $search)); ?>
			</div>
		</div>
		<div>
			<div>
				Resultados por página
			</div>
			<div>
				<?php echo Form::Select("datatable_limit_" . $name, array(
						5 => 5,
						25 => 25,
						50 => 50,
						100 => 100,
						500 => 500), 5, array(
						"data-fw-control-type" => "datatable-limit",
						"data-fw-control" => $control,
						"data-fw-control-name" => $name)); ?>
			</div>
		</div>
		<?php if (@$new === true) { ?>
		<div>
			<div>
				Nuevo
			</div>
			<div>
				<?php echo Form::Button("dt-{$name}-btn-new", '+', array("class" => "btn btn-primary btn-sm btn-mini")); ?>
			</div>
		</div>
		<?php } ?>
		<div>
			<div>
				Acciones
			</div>
			<div>
				<span class="btn-group btn-group-sm">
					<?php echo Form::Button("dt-btn-{$name}-csv", "CSV", array("class" => "btn btn-default btn-mini")); ?>
					<?php echo Form::Button("dt-btn-{$name}-print", "Print", array("class" => "btn btn-default btn-mini")); ?>
				</span>
			</div>
		</div>
	</div>
	<div id="datatable-table-<?php echo $name;?>"><?php echo @$table; ?></div>
</div>