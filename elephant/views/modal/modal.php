<div id="modal-backdrop" class="modal-backdrop">
	<div class="modal<?php echo isset($class) ? $class : null; ?>" id="<?php echo isset($name) ? $name : null; ?>">
		<div class="modal-header">
			<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
		</div>
		<div class="modal-body">
			<?php echo $content; ?>
		</div>
	</div>
</div>