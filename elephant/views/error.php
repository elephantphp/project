<?php

if (isset($link))
{
	$link_doc_error = $link . $code;
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ElephantPHP: <?php echo @$code; ?></title>
	<link rel="stylesheet" href="<?php echo URI::ElephantPHPAssets("css/style.css"); ?>">
</head>
<body class="elephantphp">
	<header>
		<nav>
			<?php echo View::Make("@{path}.elephant.views.header"); ?>
		</nav>
	</header>
	<section>
		<h1 id="error-code"><?php echo $code; ?></h1>
		<div id="error-message">
			<div class="error">
				<div class="type">Error message</div>
				<div class="text"><?php echo $message; ?></div>
			</div>
			<?php if (isset($file)) { ?>
				<div class="error">
					<div class="type">File</div>
					<div class="text"><?php echo $file; ?></div>
				</div>
			<?php } ?>
			<?php if (isset($line)) { ?>
				<div class="error">
					<div class="type">Line</div>
					<div class="text">#<?php echo $line; ?></div>
				</div>
			<?php } ?>
			<?php if (isset($link_doc_error)) { ?>
				<div class="error">
					<div class="type">More information</div>
					<div class="text">
						<a href="<?php echo $link_doc_error; ?>" target="_blank" title="Haz clic aquí para ir a la documentación de este error."><?php echo $link_doc_error; ?></a>
					</div>
				</div>
			<?php } ?>
		</div>
	</section>
</body>
</html>