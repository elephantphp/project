<ul>
	<li class="link">
		<a href="<?php echo URI::Home(); ?>">GO TO HOME</a>
	</li>
	<li class="link version" title="Versión actual">
		<a><?php echo ELEPHANTPHP_VERSION; ?></a>
	</li>
	<li class="link" title="Nombre del framework">
		<a href="<?php echo ELEPHANTPHP_LINK; ?>" target="_blank"><?php echo ELEPHANTPHP_NAME; ?></a>
	</li>
</ul>