<?php

return array(

	10000 => "No existe el controlador: @{controller} en controllers",
	10001 => "No existe el controlador: @{controller}",
	10002 => "No existe el método \"@{method}\" en el controlador: @{controller}",
	10003 => "No se encontro la página :(",
	10004 => "Error no esta definido / en las rutas"

);