<?php

/**
* @package	SimpleDBController
* @version	1.0
* @author	DavidBeru
* @since	2015-05-21
* @see		2015-05-21
*/

class SimpleDBController
{
	public function Index()
	{
		Response::Send("SimpleDBController");
	}

	public function Panel()
	{
		Response::Send("SimpleDBController Panel");
	}
}