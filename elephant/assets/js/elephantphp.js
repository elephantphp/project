$(document).ready(function()
{
	$(document).on("keyup", "input", function()
	{
		if ($(this).data("fw-control-type") == "datalist")
		{
			var control = $(this).data("fw-control");
			var name = $(this).data("fw-control-name");
			var val = $(this).val();

			if (val == '')
			{
				$("#datalist_" + name).html('');
			}
			else
			{
				$.post(url_home + "elephantphp/controls/datalist",
				{
					elephant_control: control,
					elephant_control_name: name,
					elephant_control_value: val

				}, function(data)
				{
					$("#datalist_" + name).html(data);
				});
			}
		}
	});

	$(document).on("click", "li", function()
	{
		if ($(this).data("fw-type") == "datalist")
		{
			var val = $(this).data("fw-value");
			var txt = $(this).text();
			var name = $(this).data("fw-name");

			$('#' + name).val(val);
			$("#datalist_text_" + name).val(txt);
			$("#datalist_" + name).html('');
		}
	});

	$(document).on("keyup", "input[data-fw-control-type=datatable-search]", function()
	{
		var value = $(this).val();
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_value: value
		});
	});

	var page = 1;

	$(document).on("click", "ul[data-fw-control-type=datatable-paginator] li a", function()
	{
		page = $(this).data("value");
	});

	$(document).on("click", "ul[data-fw-control-type=datatable-paginator]", function()
	{
		var value = page;
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_page: value
		});
	});

	$(document).on("change", "select[data-fw-control-type=datatable-limit]", function()
	{
		var value = $(this).val();
		var control = $(this).data("fw-control");
		var name = $(this).data("fw-control-name");
		var url = $("#datatable-url-" + name).val();

		Request.Ajax(url, "#datatable-table-" + name,
		{
			elephant_control: control,
			elephant_control_name: name,
			elephant_control_limit: value
		});
	});

	$(document).on("change", "[data-fw-control-type=\"mediaupload\"]", function()
	{
		var id = $(this).attr("id");
		var name = $(this).data("fw-control-name");
		var archivos = document.getElementById(id);
		var archivo = archivos.files;
		var formdata = new FormData();
		var multiple = $(this).attr("multiple");
		var autoload = $(this).attr("autoload");

		formdata.append("save_on", $(this).attr("save-on"));
		formdata.append("save_as", $(this).attr("save-as"));

		for (i = 0; i < archivo.length; i++)
		{
			formdata.append("file-" + i, archivo[i]);
		}

		var area_upload = $(".area-upload").html();

		$(".vista-previa").text("Wait Please, Uploading...");

		$.ajax(
		{
			url: "//" + document.domain + "/frame/controls/mediaupload",
			type: "POST",
			contentType: false,
			data: formdata,
			processData: false,
			cache: false

		}).done(function(data)
		{
			if (data.status == true)
			{
				var routes = "";
				var media = "";

				$.each(data.files, function(key, value)
				{
					routes += value["route"] + ',';
					media += value["media"] + ',';
				});

				var end = routes.length;
				var rou = routes.substr(0, (end - 1));

				var end = media.length;
				var med = media.substr(0, (end - 1));

				var med_split = med.split(',');

				$(".vista-previa").text('');

				$('#' + name).val(rou);
				$(".area-upload").html(area_upload);
				$(".area-upload-select").addClass("min");

				var files = ""

				$.each(med_split, function(key, value)
				{
					files += "<img src=\"" + value + "\">";
				});

				$(".vista-previa").html(files);
			}
			else
			{
				alert("Error, ocurrio un problema al cargar la imagen");
			}
		});
	});
});