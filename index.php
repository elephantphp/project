<?php

/**
* @package		ElephantPHP - PHP Framework
* @author		David Berumen (DavidBeru) - Dhomper Tecnologías
* @copyright	ElephantPHP is released under the MIT License
* @link			http://elephantphp.dhomper.com/
*/

(@include_once dirname(__FILE__) . "/elephant/index.php") or die ("error include elephant/index.php");